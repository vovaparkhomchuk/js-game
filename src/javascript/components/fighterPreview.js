import { createElement } from '../helpers/domHelper';
import {FighterService} from "../services/fightersService";
import {getFighterInfo} from "./fighterSelector";

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const fighterInfo = createElement({tagName: 'div',className: 'fighter-preview___info'});
  const {name, health, defense, attack, source} = fighter;
  fighterInfo.innerText = `${name}\nHealth: ${health}\nDefense: ${defense}\nAttack: ${attack}`;
  const img = createFighterImage(fighter);
  img.className += ` ${positionClassName}`;
  fighterElement.append(fighterInfo);
  fighterElement.append(img);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
