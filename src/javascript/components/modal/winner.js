import {showModal} from "./modal";
import {createElement} from "../../helpers/domHelper";
import App from '../../../../src/javascript/app';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({
    tagName: 'img',
    className: '',
    attributes: {
      src: fighter.source,
      style: "height: 50vh; width: 100%"
    }
  });

  showModal({
    title: fighter.name + ' has won!!!',
    bodyElement,
    onClose:  resetGame
  });

}

const resetGame = async () => {
  const rootElement = document.getElementById('root');
  rootElement.innerHTML = "";
  await App.startApp()
}