import {controls} from '../../constants/controls';

const isKeyDown = {}
let keys = {
  q: false,
  w: false,
  e: false,
  u: false,
  i: false,
  o: false,
};

const lastSuperHitTime = {
  first: 0,
  second: 0,
}


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    document.addEventListener('keydown', keyDownHandler);
    document.addEventListener('keyup', keyUpHandler);

    isKeyDown[firstFighter._id] = false;
    isKeyDown[secondFighter._id] = false;

    const healthBarOne = document.getElementsByClassName('arena___health-bar')[0];
    const healthBarTwo = document.getElementsByClassName('arena___health-bar')[1];
    const startHealthOne = firstFighter.health;
    const startHealthTwo = secondFighter.health;

    // console.log({healthOne, healthTwo})

    function keyDownHandler(e) {
      console.log(` ${e.code}`)

      if (e.key === "q") {
        keys.q = true;
      }
      if (e.key === "w") {
        keys.w = true;
      }

      if (e.key === "e") {
        keys.e = true;
      }

      if (e.key === "u") {
        keys.u = true;
      }

      if (e.key === "i") {
        keys.i = true;
      }

      if (e.key === "o") {
        keys.o = true;
      }

      const secsNow = Date.now() / 1000;

      if(keys.q && keys.w && keys.e){
        console.log("both the keys pressed at once")
        if (secsNow - lastSuperHitTime.first >= 10) {
          superArrack(firstFighter, secondFighter);
          lastSuperHitTime.first = Date.now() / 1000;
        }
      }

      if(keys.u && keys.i && keys.o){
        console.log("both the keys pressed at once 2")
        if (secsNow - lastSuperHitTime.second >= 10) {
          superArrack(secondFighter, firstFighter);
          lastSuperHitTime.second = Date.now() / 1000;
        }
      }

      switch (e.code) {
        case 'KeyA':
          playerAttack(firstFighter, secondFighter);
          break;
        case 'KeyJ':
          playerAttack(secondFighter, firstFighter);
          break;
        case 'KeyD':
          isKeyDown[firstFighter._id] = true;
          break;
        case 'KeyL':
          isKeyDown[secondFighter._id] = true;
          break;
      }

      healthBarOne.style.width = firstFighter.health > 0 ? (100 * firstFighter.health / startHealthOne) + '%' : 0;
      healthBarTwo.style.width = secondFighter.health > 0 ? (100 * secondFighter.health / startHealthTwo) + '%' : 0;


      // console.log({...firstFighter});
      // console.log({...secondFighter});

      if (firstFighter.health <= 0)
        resolve(secondFighter)
      if (secondFighter.health <= 0)
        resolve(firstFighter)
    }

    function keyUpHandler(e) {
      if (e.key === "q") {
        keys.q = false;
      }
      if (e.key === "w") {
        keys.w = false;
      }
      if (e.key === "e") {
        keys.e = false;
      }

      if (e.key === "u") {
        keys.u = false;
      }
      if (e.key === "i") {
        keys.i = false;
      }
      if (e.key === "o") {
        keys.o = false;
      }

      switch (e.code) {
        case 'KeyD':
          isKeyDown[firstFighter._id] = false;
          break;
        case 'KeyL':
          isKeyDown[secondFighter._id] = false;
          break;
      }
    }


    // resolve the promise with the winner when fight is over



  });
}


const playerAttack = (attacker, defender) => {
  defender.health -= getDamage(attacker, defender);
}

const superArrack = (attacker, defender) => {
  defender.health -= attacker.attack;
}

const criticalHitChance = () => {
  return getRandomArbitrary(1, 2);
}

const getRandomArbitrary = (min, max) => {
  return Math.random() * (max - min) + min;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage >= 0 ? damage : 0;
}

export function getHitPower(fighter) {
  if (isKeyDown[fighter._id]) return 0;
  return fighter.attack * criticalHitChance();
}

export function getBlockPower(fighter) {
  if (isKeyDown[fighter._id]) return 100;
  return fighter.defense * criticalHitChance();
}
